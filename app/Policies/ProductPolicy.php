<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function view(User $user, Product $product)
    {
        return $user->id > 0;
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->id > 0;
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        return $user->id === $product->user_id || $user->is_admin == 1;
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        return $user->id === $product->user_id || $user->is_admin == 1;
    }

    /**
     * Determine whether the user can restore the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function restore(User $user, Product $product)
    {
        return $user->id === $product->id || $user->is_admin == 1;
    }

    /**
     * Determine whether the user can permanently delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function forceDelete(User $user, Product $product)
    {
        return $user->id === $product->id || $user->is_admin == 1;
    }
}
