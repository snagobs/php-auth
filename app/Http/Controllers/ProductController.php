<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('auth-only', auth()->user())) {
            return view('products', ['products' => Product::all()]);
        }
        return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', Product::class)) {
            echo 'Current logged in user is allowed to create new product.';
        } else {
            echo 'Not Authorized';
        }
        exit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if (Gate::allows('auth-only', auth()->user())) {
            return view('products/{id}');
        }
        return redirect('/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if(Gate::denies('edit', $product)){
             return redirect()->back()->with('message', 'Access denied!');
           }
        return view('/products/{id}/delete');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product = Product::find($request->id);
        if(Gate::denies('update', $product)){
            return redirect()->back()->with('message', 'Access denied!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //

    }
}
